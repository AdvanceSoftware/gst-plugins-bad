/* GStreamer XAudio2 Plugin
 *
 * Copyright (C) 2018 Sebastian Dröge <sebastian@centricular.com>
 * Copyright (C) 2021 Advance Software Limited <steve@advance-software.com>
 *
 * gstxaudio2sink.h: 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * The development of this code was made possible due to the involvement
 * of Pioneers of the Inevitable, the creators of the Songbird Music player
 *
 * 
 */

#ifndef __GST_XAUDIO2SINK_H__
#define __GST_XAUDIO2SINK_H__

#include <gst/gst.h>
#include <gst/audio/audio.h>
#include <gst/audio/gstaudiosink.h>

#include <windows.h>
#include <xaudio2.h>
#include <mmreg.h> 
#include <ks.h> 
#include <ksmedia.h> 

G_BEGIN_DECLS
#define GST_TYPE_XAUDIO2_SINK            (gst_xaudio2_sink_get_type())
#define GST_XAUDIO2_SINK(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_XAUDIO2_SINK,GstXAudio2Sink))
#define GST_XAUDIO2_SINK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_XAUDIO2_SINK,GstXAudio2SinkClass))
#define GST_IS_XAUDIO2_SINK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_XAUDIO2_SINK))
#define GST_IS_XAUDIO2_SINK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_XAUDIO2_SINK))
typedef struct _GstXAudio2Sink GstXAudio2Sink;
typedef struct _GstXAudio2SinkClass GstXAudio2SinkClass;

#define GST_XAUDIO2_LOCK(obj)	  (g_mutex_lock (&obj->xaudio2_lock))
#define GST_XAUDIO2_UNLOCK(obj) (g_mutex_unlock (&obj->xaudio2_lock))


const guint GS_XAUDIO_CIRCULAR_BUFFER_COUNT = 3;


struct Voice_Callback ;


struct _GstXAudio2Sink
{
  GstAudioSink sink;

  /* XAudio2 interface pointer */
  IXAudio2 * xaudio2;
  IXAudio2MasteringVoice * mastering_voice;

  IXAudio2SourceVoice * source_voice;
  Voice_Callback * voice_cb;
  guint queued_buffer_count; /* number of buffers we currently have queued */
  guint current_buffer_index; /*next circular buffer slot to use */
  BYTE *queued_buffers[GS_XAUDIO_CIRCULAR_BUFFER_COUNT];
  guint queued_buffer_size[GS_XAUDIO_CIRCULAR_BUFFER_COUNT]; 
  
  guint bytes_per_sample;

  /* current volume setup by mixer interface */
  glong volume;
  gboolean mute;
  
  /* current xaudio2 device ID */
  gchar * device_id;

  GstCaps *cached_caps;
  
  /* lock used to protect writes and resets */
  GMutex xaudio2_lock;
};

struct _GstXAudio2SinkClass
{
  GstAudioSinkClass parent_class;
};

GType gst_xaudio2_sink_get_type (void);

#define GST_XAUDIO2_SINK_CAPS "audio/x-raw, " \
        "format = (string) S16LE, " \
        "layout = (string) interleaved, " \
        "rate = (int) [ 1, MAX ], " "channels = (int) [ 1, 2 ]; " \
        "audio/x-raw, " \
        "format = (string) U8, " \
        "layout = (string) interleaved, " \
        "rate = (int) [ 1, MAX ], " "channels = (int) [ 1, 2 ];" \
        "audio/x-ac3, framed = (boolean) true;" \
        "audio/x-dts, framed = (boolean) true;"

G_END_DECLS
#endif /* __GST_XAUDIO2SINK_H__ */
