/* GStreamer XAudio2 Plugin
 * Copyright (C) 2018 Sebastian Dröge <sebastian@centricular.com>
 * Copyright (C) 2021 Advance Software Limited <steve@advance-software.com>
 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __GST_XAUDIO2_DEVICE_H__
#define __GST_XAUDIO2_DEVICE_H__

#include <gst/gst.h>

G_BEGIN_DECLS

typedef struct _GstXAudio2DeviceProvider GstXAudio2DeviceProvider;
typedef struct _GstXAudio2DeviceProviderClass GstXAudio2DeviceProviderClass;

#define GST_TYPE_XAUDIO2_DEVICE_PROVIDER                 (gst_xaudio2_device_provider_get_type())
#define GST_IS_XAUDIO2_DEVICE_PROVIDER(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_XAUDIO2_DEVICE_PROVIDER))
#define GST_IS_XAUDIO2_DEVICE_PROVIDER_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_XAUDIO2_DEVICE_PROVIDER))
#define GST_XAUDIO2_DEVICE_PROVIDER_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_XAUDIO2_DEVICE_PROVIDER, GstXAudio2DeviceProviderClass))
#define GST_XAUDIO2_DEVICE_PROVIDER(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_XAUDIO2_DEVICE_PROVIDER, GstXAudio2DeviceProvider))
#define GST_XAUDIO2_DEVICE_PROVIDER_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_DEVICE_PROVIDER, GstXAudio2DeviceProviderClass))
#define GST_XAUDIO2_DEVICE_PROVIDER_CAST(obj)            ((GstXAudio2DeviceProvider *)(obj))

struct _GstXAudio2DeviceProvider {
  GstDeviceProvider parent;
};

struct _GstXAudio2DeviceProviderClass {
  GstDeviceProviderClass parent_class;
};

GType gst_xaudio2_device_provider_get_type (void);


typedef struct _GstXAudio2Device GstXAudio2Device;
typedef struct _GstXAudio2DeviceClass GstXAudio2DeviceClass;

#define GST_TYPE_XAUDIO2_DEVICE                 (gst_xaudio2_device_get_type())
#define GST_IS_XAUDIO2_DEVICE(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_XAUDIO2_DEVICE))
#define GST_IS_XAUDIO2_DEVICE_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_XAUDIO2_DEVICE))
#define GST_XAUDIO2_DEVICE_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_XAUDIO2_DEVICE, GstXAudio2DeviceClass))
#define GST_XAUDIO2_DEVICE(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_XAUDIO2_DEVICE, GstXAudio2Device))
#define GST_XAUDIO2_DEVICE_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_DEVICE, GstXAudio2DeviceClass))
#define GST_XAUDIO2_DEVICE_CAST(obj)            ((GstXAudio2Device *)(obj))

struct _GstXAudio2Device {
  GstDevice parent;

  gchar *guid;
};

struct _GstXAudio2DeviceClass {
  GstDeviceClass parent_class;
};

GType gst_xaudio2_device_get_type (void);

G_END_DECLS

#endif /* __GST_XAUDIO2_DEVICE_H__ */
