/* GStreamer XAudio2 sink
*
 * Copyright (C) 2018 Sebastian Dröge <sebastian@centricular.com>
 * Copyright (C) 2021 Advance Software Limited <steve@advance-software.com>
*
* gstxaudio2sink.cpp:
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.

* You should have received a copy of the GNU Lesser General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/

/**
 * SECTION:element-xaudio2sink
 * @title: xaudio2sink
 *
 * This skink output's sound using the XAudio2 API.
 *
 * You should almost always use generic audio conversion elements
 * like audioconvert and audioresample in front of an audiosink to make sure
 * your pipeline works under all circumstances - those conversion elements will
 * act in passthrough mode if no conversion is necessary.
 *
 * ## Example pipelines
 * |[
 * gst-launch-1.0 -v audiotestsrc ! audioconvert ! volume volume=0.1 ! xaudio2sink
 * ]| will output a sine wave (continuous beep sound) to your sound card 
 * (at very low volume as precaution).
 * |[
 * gst-launch-1.0 -v filesrc location=music.ogg ! decodebin ! audioconvert ! audioresample ! xaudiosink
 * ]| will play an Ogg/Vorbis audio file and output it.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

extern "C"
{
#include <gst/audio/gstaudioiec61937.h>
}

#include <gst/base/gstbasesink.h>
#include "gstxaudio2sink.h"

#include <math.h>

#ifdef __CYGWIN__
#include <unistd.h>
#ifndef _swab
#define _swab swab
#endif
#endif

#define DEFAULT_MUTE FALSE

GST_DEBUG_CATEGORY_STATIC (xaudio2sink_debug);
#define GST_CAT_DEFAULT xaudio2sink_debug

static void gst_xaudio2_sink_finalize (GObject * object);

static void gst_xaudio2_sink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_xaudio2_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static GstCaps *gst_xaudio2_sink_getcaps (GstBaseSink * bsink,
    GstCaps * filter);
static GstBuffer *gst_xaudio2_sink_payload (GstAudioBaseSink * sink,
    GstBuffer * buf);
static gboolean gst_xaudio2_sink_prepare (GstAudioSink * asink,
    GstAudioRingBufferSpec * spec);
static gboolean gst_xaudio2_sink_unprepare (GstAudioSink * asink);
static gboolean gst_xaudio2_sink_open (GstAudioSink * asink);
static gboolean gst_xaudio2_sink_close (GstAudioSink * asink);
static gint gst_xaudio2_sink_write (GstAudioSink * asink,
    gpointer data, guint length);
static guint gst_xaudio2_sink_delay (GstAudioSink * asink);
static void gst_xaudio2_sink_reset (GstAudioSink * asink);
static GstCaps *gst_xaudio2_probe_supported_formats (GstXAudio2Sink *
    sink_xaudio2, const GstCaps * template_caps);
static gboolean gst_xaudio2_sink_query (GstBaseSink * pad,
    GstQuery * query);

static void gst_xaudio2_sink_set_volume (GstXAudio2Sink * sink,
    gdouble volume, gboolean store);
static gdouble gst_xaudio2_sink_get_volume (GstXAudio2Sink * sink);
static void gst_xaudio2_sink_set_mute (GstXAudio2Sink * sink,
    gboolean mute);
static gboolean gst_xaudio2_sink_get_mute (GstXAudio2Sink * sink);
static const gchar *gst_xaudio2_sink_get_device (GstXAudio2Sink *
    sink_xaudio2);
static void gst_xaudio2_sink_set_device (GstXAudio2Sink * sink_xaudio2,
    const gchar * device_id);

static gboolean gst_xaudio2_sink_is_spdif_format (GstAudioRingBufferSpec *
    spec);

static gchar *gst_hres_to_string (HRESULT hRes);

static GstStaticPadTemplate xaudio2sink_sink_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_XAUDIO2_SINK_CAPS));

enum
{
  PROP_0,
  PROP_VOLUME,
  PROP_MUTE,
  PROP_DEVICE
};

#define gst_xaudio2_sink_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE (GstXAudio2Sink, gst_xaudio2_sink,
    GST_TYPE_AUDIO_SINK, G_IMPLEMENT_INTERFACE (GST_TYPE_STREAM_VOLUME, NULL)
    );

static void
gst_xaudio2_sink_finalize (GObject * object)
{
  GstXAudio2Sink *sink_xaudio2 = GST_XAUDIO2_SINK (object);

  g_free (sink_xaudio2->device_id);
  sink_xaudio2->device_id = NULL;

  g_mutex_clear (&sink_xaudio2->xaudio2_lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_xaudio2_sink_class_init (GstXAudio2SinkClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseSinkClass *gstbasesink_class = GST_BASE_SINK_CLASS (klass);
  GstAudioSinkClass *gstaudiosink_class = GST_AUDIO_SINK_CLASS (klass);
  GstAudioBaseSinkClass *gstaudiobasesink_class =
      GST_AUDIO_BASE_SINK_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  GST_DEBUG_CATEGORY_INIT (xaudio2sink_debug, "xaudio2sink", 0,
      "XAudio2 sink");

  gobject_class->finalize = gst_xaudio2_sink_finalize;
  gobject_class->set_property = gst_xaudio2_sink_set_property;
  gobject_class->get_property = gst_xaudio2_sink_get_property;

  gstbasesink_class->get_caps =
      GST_DEBUG_FUNCPTR (gst_xaudio2_sink_getcaps);

  gstbasesink_class->query = GST_DEBUG_FUNCPTR (gst_xaudio2_sink_query);

  gstaudiobasesink_class->payload =
      GST_DEBUG_FUNCPTR (gst_xaudio2_sink_payload);

  gstaudiosink_class->prepare =
      GST_DEBUG_FUNCPTR (gst_xaudio2_sink_prepare);
  gstaudiosink_class->unprepare =
      GST_DEBUG_FUNCPTR (gst_xaudio2_sink_unprepare);
  gstaudiosink_class->open = GST_DEBUG_FUNCPTR (gst_xaudio2_sink_open);
  gstaudiosink_class->close = GST_DEBUG_FUNCPTR (gst_xaudio2_sink_close);
  gstaudiosink_class->write = GST_DEBUG_FUNCPTR (gst_xaudio2_sink_write);
  gstaudiosink_class->delay = GST_DEBUG_FUNCPTR (gst_xaudio2_sink_delay);
  gstaudiosink_class->reset = GST_DEBUG_FUNCPTR (gst_xaudio2_sink_reset);

  g_object_class_install_property (gobject_class,
      PROP_VOLUME,
      g_param_spec_double ("volume", "Volume",
          "Volume of this stream", 0.0, 1.0, 1.0,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS) ));

  g_object_class_install_property (gobject_class,
      PROP_MUTE,
      g_param_spec_boolean ("mute", "Mute",
          "Mute state of this stream", DEFAULT_MUTE,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS) ));

  g_object_class_install_property (gobject_class,
      PROP_DEVICE,
      g_param_spec_string ("device", "Device",
          "XAudio2 playback device as a GUID string",
          NULL, (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS) ));

  gst_element_class_set_static_metadata (element_class,
      "XAudio2 Audio Sink", "Sink/Audio",
      "Output to sound processor via XAudio2",
      "Steve Williams <steve@advance-software.com>");

  gst_element_class_add_static_pad_template (element_class,
      &xaudio2sink_sink_factory);
}

static void
gst_xaudio2_sink_init (GstXAudio2Sink * sink_xaudio2)
{
  sink_xaudio2->volume = 100;
  sink_xaudio2->mute = FALSE;
  sink_xaudio2->device_id = NULL;
  sink_xaudio2->xaudio2 = NULL;
  sink_xaudio2->mastering_voice = NULL;
  sink_xaudio2->source_voice = NULL;
  sink_xaudio2->voice_cb = NULL;
  sink_xaudio2->current_buffer_index = 0;
  sink_xaudio2->queued_buffer_count = 0;
  
  sink_xaudio2->cached_caps = NULL;
  
  for (int i=0;i<GS_XAUDIO_CIRCULAR_BUFFER_COUNT;i++)
  {
     sink_xaudio2->queued_buffer_size[i] = 0;
     sink_xaudio2->queued_buffers[i] = NULL;
  }
  
  g_mutex_init (&sink_xaudio2->xaudio2_lock);
}


static void
gst_xaudio2_sink_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  GstXAudio2Sink *sink = GST_XAUDIO2_SINK (object);

  switch (prop_id) {
    case PROP_VOLUME:
      gst_xaudio2_sink_set_volume (sink, g_value_get_double (value), TRUE);
      break;
    case PROP_MUTE:
      gst_xaudio2_sink_set_mute (sink, g_value_get_boolean (value));
      break;
    case PROP_DEVICE:
      gst_xaudio2_sink_set_device (sink, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_xaudio2_sink_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstXAudio2Sink *sink = GST_XAUDIO2_SINK (object);

  switch (prop_id) {
    case PROP_VOLUME:
      g_value_set_double (value, gst_xaudio2_sink_get_volume (sink));
      break;
    case PROP_MUTE:
      g_value_set_boolean (value, gst_xaudio2_sink_get_mute (sink));
      break;
    case PROP_DEVICE:
      g_value_set_string (value, gst_xaudio2_sink_get_device (sink));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstCaps *
gst_xaudio2_sink_getcaps (GstBaseSink * bsink, GstCaps * filter)
{
  GstElementClass *element_class;
  GstPadTemplate *pad_template;
  GstXAudio2Sink *sink_xaudio2 = GST_XAUDIO2_SINK (bsink);
  GstCaps *caps;

  if (sink_xaudio2->xaudio2 == NULL) {
    GST_DEBUG_OBJECT (sink_xaudio2, "device not open, using template caps");
    return NULL;                /* base class will get template caps for us */
  }

// ADV_SW[PORT]


  if (sink_xaudio2->cached_caps) {
    caps = gst_caps_ref (sink_xaudio2->cached_caps);
  } else {
    element_class = GST_ELEMENT_GET_CLASS (sink_xaudio2);
    pad_template = gst_element_class_get_pad_template (element_class, "sink");
    g_return_val_if_fail (pad_template != NULL, NULL);

    caps = gst_xaudio2_probe_supported_formats (sink_xaudio2,
        gst_pad_template_get_caps (pad_template));
    if (caps)
      sink_xaudio2->cached_caps = gst_caps_ref (caps);
  }

  if (caps && filter) {
    GstCaps *tmp =
        gst_caps_intersect_full (filter, caps, GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (caps);
    caps = tmp;
  }

  if (caps) {
    gchar *caps_string = gst_caps_to_string (caps);
    GST_DEBUG_OBJECT (sink_xaudio2, "returning caps %s", caps_string);
    g_free (caps_string);
  }

  return caps;
}

static gboolean
gst_xaudio2_sink_acceptcaps (GstBaseSink * sink, GstQuery * query)
{
  GstXAudio2Sink *dsink = GST_XAUDIO2_SINK (sink);
  GstPad *pad;
  GstCaps *caps;
  GstCaps *pad_caps;
  GstStructure *st;
  gboolean ret = FALSE;
  GstAudioRingBufferSpec spec = { 0 };

  if (G_UNLIKELY (dsink == NULL))
    return FALSE;

  pad = sink->sinkpad;

  gst_query_parse_accept_caps (query, &caps);
  GST_DEBUG_OBJECT (pad, "caps %" GST_PTR_FORMAT, caps);

  pad_caps = gst_pad_query_caps (pad, NULL);
  if (pad_caps) {
    gboolean cret = gst_caps_is_subset (caps, pad_caps);
    gst_caps_unref (pad_caps);
    if (!cret) {
      GST_DEBUG_OBJECT (dsink,
          "Caps are not a subset of the pad caps, not accepting caps");
      goto done;
    }
  }

  /* If we've not got fixed caps, creating a stream might fail, so let's just
   * return from here with default acceptcaps behaviour */
  if (!gst_caps_is_fixed (caps)) {
    GST_DEBUG_OBJECT (dsink, "Caps are not fixed, not accepting caps");
    goto done;
  }

  spec.latency_time = GST_SECOND;
  if (!gst_audio_ring_buffer_parse_caps (&spec, caps)) {
    GST_DEBUG_OBJECT (dsink, "Failed to parse caps, not accepting");
    goto done;
  }

  /* Make sure input is framed (one frame per buffer) and can be payloaded */
  switch (spec.type) {
    case GST_AUDIO_RING_BUFFER_FORMAT_TYPE_AC3:
    case GST_AUDIO_RING_BUFFER_FORMAT_TYPE_DTS:
    {
      gboolean framed = FALSE, parsed = FALSE;
      st = gst_caps_get_structure (caps, 0);

      gst_structure_get_boolean (st, "framed", &framed);
      gst_structure_get_boolean (st, "parsed", &parsed);
      if ((!framed && !parsed) || gst_audio_iec61937_frame_size (&spec) <= 0) {
        GST_DEBUG_OBJECT (dsink, "Wrong AC3/DTS caps, not accepting");
        goto done;
      }
    }
    default:
      break;
  }
  ret = TRUE;
  GST_DEBUG_OBJECT (dsink, "Accepting caps");

done:
  gst_query_set_accept_caps_result (query, ret);
  return TRUE;
}

static gboolean
gst_xaudio2_sink_query (GstBaseSink * sink, GstQuery * query)
{
  gboolean res = TRUE;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_ACCEPT_CAPS:
      res = gst_xaudio2_sink_acceptcaps (sink, query);
      break;
    default:
      res = GST_BASE_SINK_CLASS (parent_class)->query (sink, query);
  }

  return res;
}

static LPGUID
string_to_guid (const gchar * str)
{
  HRESULT ret;
  gunichar2 *wstr;
  LPGUID out;

  wstr = g_utf8_to_utf16 (str, -1, NULL, NULL, NULL);
  if (!wstr)
    return NULL;

  out = g_new (GUID, 1);
  ret = CLSIDFromString ((LPOLESTR) wstr, out);
  g_free (wstr);
  if (ret != NOERROR) {
    g_free (out);
    return NULL;
  }

  return out;
}


static gboolean
gst_xaudio2_sink_open (GstAudioSink * asink)
{
  HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    
  GstXAudio2Sink *sink_xaudio2;
  LPGUID lpGuid = NULL;

  sink_xaudio2 = GST_XAUDIO2_SINK (asink);

  if (sink_xaudio2->device_id) {
    lpGuid = string_to_guid (sink_xaudio2->device_id);
    if (lpGuid == NULL) {
      GST_ELEMENT_ERROR (sink_xaudio2, RESOURCE, OPEN_READ,
          ("device set but guid not found: %s", sink_xaudio2->device_id), (NULL));
      return FALSE;
    }
  }

   // XAudio2 interface init

   // Workaround for XAudio 2.7 known issue
#ifdef _DEBUG
   HMODULE XAudioDLL = LoadLibraryExW(L"XAudioD2_7.DLL", nullptr, 0x00000800 /* LOAD_LIBRARY_SEARCH_SYSTEM32 */);
#else
   HMODULE XAudioDLL = LoadLibraryExW(L"XAudio2_7.DLL", nullptr, 0x00000800 /* LOAD_LIBRARY_SEARCH_SYSTEM32 */);
#endif // _DEBUG

   if (!XAudioDLL)
   {
      GST_ELEMENT_ERROR (sink_xaudio2, RESOURCE, OPEN_READ,
        ("Failed to find XAudio 2.7 DLL"), (NULL));
      
      CoUninitialize();
      return FALSE;
   }


   UINT32 flags = 0;

#if defined(USING_XAudio2_7_DIRECTX) && defined(_DEBUG)
   flags |= XAudio2_DEBUG_ENGINE;
#endif

   hr = XAudio2Create(&sink_xaudio2->xaudio2, flags);
   if (hr != S_OK)
   {
      gchar *error_text = gst_hres_to_string (hr);
      GST_ELEMENT_ERROR (sink_xaudio2, RESOURCE, OPEN_READ,
        ("Failed to init XAudio2 engine: %s", error_text), (NULL));
      g_free (error_text);
    
      CoUninitialize();
      return FALSE;
   }

#if 0 // !defined(USING_XAUDIO2_7_DIRECTX) && defined(_DEBUG)
   // To see the trace output, you need to view ETW logs for this application:
   //  Go to Control Panel, Administrative Tools, Event Viewer.
   //  View->Show Analytic and Debug Logs.
   //  Applications and Services Logs / Microsoft / Windows / XAudio2. 
   //  Right click on Microsoft Windows XAudio2 debug logging,  
   //  Properties, then Enable Logging and hit OK.
   XAUDIO2_DEBUG_CONFIGURATION debug = { 0 };
   debug.TraceMask = XAudio2_LOG_ERRORS | XAudio2_LOG_WARNINGS;
   debug.BreakMask = XAudio2_LOG_ERRORS;
   xaudio2->SetDebugConfiguration(&debug, 0);
#endif

   // Create a mastering voice
   hr = sink_xaudio2->xaudio2->CreateMasteringVoice(&sink_xaudio2->mastering_voice);
   
   if (hr != S_OK)
   {
      gchar *error_text = gst_hres_to_string (hr);
      GST_ELEMENT_ERROR (sink_xaudio2, RESOURCE, OPEN_READ,
        ("Failed creating mastering voice: %s", error_text), (NULL));
      g_free (error_text);      

      sink_xaudio2->xaudio2->Release();
      CoUninitialize();
      return FALSE;
   }

  return TRUE;
}

static gboolean
gst_xaudio2_sink_is_spdif_format (GstAudioRingBufferSpec * spec)
{
  return spec->type == GST_AUDIO_RING_BUFFER_FORMAT_TYPE_AC3 ||
      spec->type == GST_AUDIO_RING_BUFFER_FORMAT_TYPE_DTS;
}


//---------------------------------
// XAudio voice callback structures
//---------------------------------

class Payload
{
public:
   GstXAudio2Sink * m_owner;
   void * m_data;
};


struct Voice_Callback : public IXAudio2VoiceCallback
{
   STDMETHOD_(void, OnVoiceProcessingPassStart)(UINT32) override
   {
   }
   STDMETHOD_(void, OnVoiceProcessingPassEnd)() override
   {
   }
   STDMETHOD_(void, OnStreamEnd)() override
   {
   }
   STDMETHOD_(void, OnBufferStart)(void*) override
   {
   }
   
   STDMETHOD_(void, OnBufferEnd)(void *user_data) override
   {
      if (user_data)
      {
         // TODO: Consider whether this needs a mutex - seems ok without one.
         Payload *pl = (Payload*) user_data;
         pl->m_owner->queued_buffer_count--;
         delete pl;
      }

      SetEvent(hBufferEndEvent);
   }

   STDMETHOD_(void, OnLoopEnd)(void*) override
   {
   }
   STDMETHOD_(void, OnVoiceError)(void*, HRESULT) override
   {
   }

   HANDLE hBufferEndEvent;

   Voice_Callback() :
#if (_WIN32_WINNT >= _WIN32_WINNT_VISTA)
      hBufferEndEvent(CreateEventEx(nullptr, nullptr, 0, EVENT_MODIFY_STATE | SYNCHRONIZE))
#else
      hBufferEndEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr))
#endif
   {
   }
   virtual ~Voice_Callback()
   {
      CloseHandle(hBufferEndEvent);
   }
};


static gboolean
gst_xaudio2_sink_prepare (GstAudioSink * asink,
    GstAudioRingBufferSpec * spec)
{
  GstXAudio2Sink *sink_xaudio2;
  WAVEFORMATEX wfx;

  sink_xaudio2 = GST_XAUDIO2_SINK (asink);

  /*save number of bytes per sample and buffer format */
  sink_xaudio2->bytes_per_sample = spec->info.bpf;

  /* fill the WAVEFORMATEX structure with spec params */
  memset (&wfx, 0, sizeof (wfx));
  if (!gst_xaudio2_sink_is_spdif_format (spec)) {
    wfx.cbSize = sizeof (wfx);
    wfx.wFormatTag = WAVE_FORMAT_PCM;
    wfx.nChannels = spec->info.channels;
    wfx.nSamplesPerSec = spec->info.rate;
    wfx.wBitsPerSample = (spec->info.bpf * 8) / wfx.nChannels;
    wfx.nBlockAlign = spec->info.bpf;
    wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;

    /* Create buffer with size based on our configured
     * buffer_size (which is 200 ms by default) */
    guint buffer_size =
        gst_util_uint64_scale_int (wfx.nAvgBytesPerSec, spec->buffer_time,
        GST_MSECOND);
    /* Make sure we make those numbers multiple of our sample size in bytes */
    buffer_size -= buffer_size % spec->info.bpf;

    spec->segsize =
        gst_util_uint64_scale_int (wfx.nAvgBytesPerSec, spec->latency_time,
        GST_MSECOND);
    spec->segsize -= spec->segsize % spec->info.bpf;
    spec->segtotal = buffer_size / spec->segsize;
  } else {
#ifdef WAVE_FORMAT_DOLBY_AC3_SPDIF
    wfx.cbSize = 0;
    wfx.wFormatTag = WAVE_FORMAT_DOLBY_AC3_SPDIF;
    wfx.nChannels = 2;
    wfx.nSamplesPerSec = 48000;
    wfx.wBitsPerSample = 16;
    wfx.nBlockAlign = wfx.wBitsPerSample / 8 * wfx.nChannels;
    wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;

    spec->segsize = 6144;
    spec->segtotal = 10;
#else
    g_assert_not_reached ();
#endif
  }

  // Make the final buffer size be an integer number of segments
  guint buffer_size = spec->segsize * spec->segtotal;

  GST_INFO_OBJECT (sink_xaudio2, "channels: %d, rate: %d, bytes_per_sample: %d"
      " WAVEFORMATEX.nSamplesPerSec: %ld, WAVEFORMATEX.wBitsPerSample: %d,"
      " WAVEFORMATEX.nBlockAlign: %d, WAVEFORMATEX.nAvgBytesPerSec: %ld\n"
      "Size of xaudio circular buffer=>%d\n",
      GST_AUDIO_INFO_CHANNELS (&spec->info), GST_AUDIO_INFO_RATE (&spec->info),
      GST_AUDIO_INFO_BPF (&spec->info), wfx.nSamplesPerSec, wfx.wBitsPerSample,
      wfx.nBlockAlign, wfx.nAvgBytesPerSec, buffer_size);

   // Create the source voice
 
   sink_xaudio2->voice_cb = new Voice_Callback;
   
   HRESULT hr = sink_xaudio2->xaudio2->CreateSourceVoice(&sink_xaudio2->source_voice, &wfx, 0, 1.0f, sink_xaudio2->voice_cb);


   if (FAILED (hr)) {
     gchar *error_text = gst_hres_to_string (hr);
     GST_ELEMENT_ERROR (sink_xaudio2, RESOURCE, OPEN_READ,
        ("XAudio2 CreateSourceVoice: %s", error_text), (NULL));
     g_free (error_text);
     return FALSE;
  }

  sink_xaudio2->source_voice->Start(0, 0);


  gst_xaudio2_sink_set_volume (sink_xaudio2,
      gst_xaudio2_sink_get_volume (sink_xaudio2), FALSE);
  gst_xaudio2_sink_set_mute (sink_xaudio2, sink_xaudio2->mute);

  return TRUE;
}

static gboolean
gst_xaudio2_sink_unprepare (GstAudioSink * asink)
{
  GstXAudio2Sink *sink_xaudio2;

  sink_xaudio2 = GST_XAUDIO2_SINK (asink);

  // Audio voice release.
  
  // ADV_SW[3]: Unclear what goes here vs [1 & 2]
  
  delete sink_xaudio2->voice_cb;
  sink_xaudio2->voice_cb = NULL;

  sink_xaudio2->source_voice->DestroyVoice();
  sink_xaudio2->source_voice = NULL;
  
  for (int i=0;i<GS_XAUDIO_CIRCULAR_BUFFER_COUNT;i++)
  {
     sink_xaudio2->queued_buffer_size[i] = 0;
     
     if (sink_xaudio2->queued_buffers[i])
     {
        delete [] sink_xaudio2->queued_buffers[i];
        sink_xaudio2->queued_buffers[i] = NULL;
     }
  }

  return TRUE;
}

static gboolean
gst_xaudio2_sink_close (GstAudioSink * asink)
{
  GstXAudio2Sink *sink_xaudio2 = NULL;

  sink_xaudio2 = GST_XAUDIO2_SINK (asink);

  /* release XAudio2 object */
  
  // ADV_SW[1]: Unclear what goes here vs [2 & 3]

  gst_caps_replace (&sink_xaudio2->cached_caps, NULL);

  return TRUE;
}


static void Submit(GstXAudio2Sink *sink_xaudio2, BYTE *data, guint len, void *user_data)
{
   XAUDIO2_BUFFER buf = { 0 };
   buf.AudioBytes = len;
   buf.pAudioData = data;

   sink_xaudio2->queued_buffer_count++;
   
   auto *t = new Payload;
   t->m_owner = sink_xaudio2;
   t->m_data = user_data;
   
   buf.pContext = t;
   sink_xaudio2->source_voice->SubmitSourceBuffer(&buf);
}


static bool Pull(GstXAudio2Sink *sink_xaudio2, BYTE *data, guint sample_buffer_length)
{
   if (sink_xaudio2->queued_buffer_size[sink_xaudio2->current_buffer_index] < sample_buffer_length)
   {
      if (sink_xaudio2->queued_buffers[sink_xaudio2->current_buffer_index]) 
         delete [] sink_xaudio2->queued_buffers[sink_xaudio2->current_buffer_index];
      
      sink_xaudio2->queued_buffers[sink_xaudio2->current_buffer_index] = new BYTE[sample_buffer_length];
      
      sink_xaudio2->queued_buffer_size[sink_xaudio2->current_buffer_index] = sample_buffer_length;
   }

   memcpy_s(sink_xaudio2->queued_buffers[sink_xaudio2->current_buffer_index], sample_buffer_length, data, sample_buffer_length);

   // Wait to see if our XAudio2 source voice has played enough data for us to give
   // it another buffer full of audio. We'd like to keep no more than MAX_BUFFER_COUNT - 1
   // buffers on the queue, so that one buffer is always available.

   XAUDIO2_VOICE_STATE state;

   for (;;)
   {
      sink_xaudio2->source_voice->GetState(&state);

      if (state.BuffersQueued < GS_XAUDIO_CIRCULAR_BUFFER_COUNT - 1)
      {
         break;
      }

      ::WaitForSingleObject(sink_xaudio2->voice_cb->hBufferEndEvent, INFINITE);
   }

   Submit(sink_xaudio2, sink_xaudio2->queued_buffers[sink_xaudio2->current_buffer_index], sample_buffer_length, nullptr);

   sink_xaudio2->current_buffer_index++;
   sink_xaudio2->current_buffer_index %= GS_XAUDIO_CIRCULAR_BUFFER_COUNT;

   return true;
}


static gint
gst_xaudio2_sink_write (GstAudioSink * asink, gpointer data, guint length)
{
  GstXAudio2Sink *sink_xaudio2 = GST_XAUDIO2_SINK (asink);

  GST_XAUDIO2_LOCK (sink_xaudio2);

  Pull(sink_xaudio2, (BYTE *) data, length);

  GST_XAUDIO2_UNLOCK (sink_xaudio2);
 
  return length;
}

static guint
gst_xaudio2_sink_delay (GstAudioSink * asink)
{
  GstXAudio2Sink *sink_xaudio2;

  sink_xaudio2 = GST_XAUDIO2_SINK (asink);

  // ADV_SW[PORT]

  // TODO: What is purpose of this callback ?

  return 0;
}

static void
gst_xaudio2_sink_reset (GstAudioSink * asink)
{
  GstXAudio2Sink *sink_xaudio2;

  sink_xaudio2 = GST_XAUDIO2_SINK (asink);

  GST_XAUDIO2_LOCK (sink_xaudio2);

  // ADV_SW[2]: Unclear what goes here vs [1 & 3]

  sink_xaudio2->mastering_voice->DestroyVoice();
  sink_xaudio2->xaudio2->Release();

  GST_XAUDIO2_UNLOCK (sink_xaudio2);
}

/*
 * gst_xaudio2_probe_supported_formats:
 *
 * Takes the template caps and returns the subset which is actually
 * supported by this device.
 *
 */

static GstCaps *
gst_xaudio2_probe_supported_formats (GstXAudio2Sink * sink_xaudio2,
    const GstCaps * template_caps)
{

 // ADV_SW[PORT]


  //WAVEFORMATEX wfx;
  GstCaps *caps;
  GstCaps *tmp, *tmp2;


  caps = gst_caps_copy (template_caps);

  /*
   * Check availability of digital output by trying to create an SPDIF buffer
   */

// ADV_SW[PORT]

#if 0 // def WAVE_FORMAT_DOLBY_AC3_SPDIF
  /* fill the WAVEFORMATEX structure with some standard AC3 over SPDIF params */
  memset (&wfx, 0, sizeof (wfx));
  wfx.cbSize = 0;
  wfx.wFormatTag = WAVE_FORMAT_DOLBY_AC3_SPDIF;
  wfx.nChannels = 2;
  wfx.nSamplesPerSec = 48000;
  wfx.wBitsPerSample = 16;
  wfx.nBlockAlign = 4;
  wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;

  // create a secondary xaudio2 buffer
  memset (&descSecondary, 0, sizeof (DSBUFFERDESC));
  descSecondary.dwSize = sizeof (DSBUFFERDESC);
  descSecondary.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_GLOBALFOCUS;
  descSecondary.dwBufferBytes = 6144;
  descSecondary.lpwfxFormat = &wfx;

  hRes = IXAudio2_CreateSoundBuffer (sink_xaudio2->pDS, &descSecondary,
      &tmpBuffer, NULL);
  if (FAILED (hRes)) {
    gchar *error_text = gst_hres_to_string (hRes);
    GST_INFO_OBJECT (sink_xaudio2, "AC3 passthrough not supported "
        "(IXAudio2_CreateSoundBuffer returned: %s)\n", error_text);
    g_free (error_text);
    tmp = gst_caps_new_empty_simple ("audio/x-ac3");
    tmp2 = gst_caps_subtract (caps, tmp);
    gst_caps_unref (tmp);
    gst_caps_unref (caps);
    caps = tmp2;
    tmp = gst_caps_new_empty_simple ("audio/x-dts");
    tmp2 = gst_caps_subtract (caps, tmp);
    gst_caps_unref (tmp);
    gst_caps_unref (caps);
    caps = tmp2;
  } else 
  {
    GST_INFO_OBJECT (sink_xaudio2, "AC3 passthrough supported");
    hRes = IXAudio2Buffer_Release (tmpBuffer);
    if (FAILED (hRes)) {
      gchar *error_text = gst_hres_to_string (hRes);
      GST_DEBUG_OBJECT (sink_xaudio2,
          "(IXAudio2Buffer_Release returned: %s)\n", error_text);
      g_free (error_text);
    }
  }
#else
  tmp = gst_caps_new_empty_simple ("audio/x-ac3");
  tmp2 = gst_caps_subtract (caps, tmp);
  gst_caps_unref (tmp);
  gst_caps_unref (caps);
  caps = tmp2;
  tmp = gst_caps_new_empty_simple ("audio/x-dts");
  tmp2 = gst_caps_subtract (caps, tmp);
  gst_caps_unref (tmp);
  gst_caps_unref (caps);
  caps = tmp2;
#endif


  return caps;
}

static GstBuffer *
gst_xaudio2_sink_payload (GstAudioBaseSink * sink, GstBuffer * buf)
{
  if (gst_xaudio2_sink_is_spdif_format (&sink->ringbuffer->spec)) {
    gint framesize = gst_audio_iec61937_frame_size (&sink->ringbuffer->spec);
    GstBuffer *out;
    GstMapInfo infobuf, infoout;
    gboolean success;

    if (framesize <= 0)
      return NULL;

    out = gst_buffer_new_and_alloc (framesize);

    if (!gst_buffer_map (buf, &infobuf, GST_MAP_READWRITE)) {
      gst_buffer_unref (out);
      return NULL;
    }
    if (!gst_buffer_map (out, &infoout, GST_MAP_READWRITE)) {
      gst_buffer_unmap (buf, &infobuf);
      gst_buffer_unref (out);
      return NULL;
    }
    success = gst_audio_iec61937_payload (infobuf.data, infobuf.size,
        infoout.data, infoout.size, &sink->ringbuffer->spec, G_BYTE_ORDER);
    if (!success) {
      gst_buffer_unmap (out, &infoout);
      gst_buffer_unmap (buf, &infobuf);
      gst_buffer_unref (out);
      return NULL;
    }

    gst_buffer_copy_into (out, buf, GST_BUFFER_COPY_ALL, 0, -1);
    /* Fix endianness */
    _swab ((gchar *) infoout.data, (gchar *) infoout.data, infobuf.size);
    gst_buffer_unmap (out, &infoout);
    gst_buffer_unmap (buf, &infobuf);
    return out;
  } else
    return gst_buffer_ref (buf);
}

static void
gst_xaudio2_sink_set_volume (GstXAudio2Sink * sink_xaudio2,
    gdouble dvolume, gboolean store)
{
  glong volume;

  volume = dvolume * 100;
  if (store)
    sink_xaudio2->volume = volume;

// ADV_SW[PORT]
#if 0

  if (sink_xaudio2->pDSBSecondary) {
    /* XAudio2 controls volume using units of 100th of a decibel,
     * ranging from -10000 to 0. We use a linear scale of 0 - 100
     * here, so remap.
     */
    long dsVolume;
    if (volume == 0 || sink_xaudio2->mute)
      dsVolume = -10000;
    else
      dsVolume = 100 * (long) (20 * log10 ((double) volume / 100.));
    dsVolume = CLAMP (dsVolume, -10000, 0);

    GST_DEBUG_OBJECT (sink_xaudio2,
        "Setting volume on secondary buffer to %d from %d", (int) dsVolume,
        (int) volume);
    IXAudio2Buffer_SetVolume (sink_xaudio2->pDSBSecondary, dsVolume);
  }
#endif  
  
}

gdouble
gst_xaudio2_sink_get_volume (GstXAudio2Sink * sink_xaudio2)
{
  return (gdouble) sink_xaudio2->volume / 100;
}

static void
gst_xaudio2_sink_set_mute (GstXAudio2Sink * sink_xaudio2, gboolean mute)
{
  if (mute) {
    gst_xaudio2_sink_set_volume (sink_xaudio2, 0, FALSE);
    sink_xaudio2->mute = TRUE;
  } else {
    gst_xaudio2_sink_set_volume (sink_xaudio2,
        gst_xaudio2_sink_get_volume (sink_xaudio2), FALSE);
    sink_xaudio2->mute = FALSE;
  }

}

static gboolean
gst_xaudio2_sink_get_mute (GstXAudio2Sink * sink_xaudio2)
{
  return sink_xaudio2->mute;
}

static const gchar *
gst_xaudio2_sink_get_device (GstXAudio2Sink * sink_xaudio2)
{
  return sink_xaudio2->device_id;
}

static void
gst_xaudio2_sink_set_device (GstXAudio2Sink * sink_xaudio2,
    const gchar * device_id)
{
  g_free (sink_xaudio2->device_id);
  sink_xaudio2->device_id = g_strdup (device_id);
}

/* Converts a HRESULT error to a text string
 * LPTSTR is either a */
static gchar *
gst_hres_to_string (HRESULT hRes)
{
  DWORD flags;
  gchar *ret_text;
  LPTSTR error_text = NULL;

  flags = FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER
      | FORMAT_MESSAGE_IGNORE_INSERTS;
  FormatMessage (flags, NULL, hRes, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),
      (LPTSTR) & error_text, 0, NULL);

#ifdef UNICODE
  /* If UNICODE is defined, LPTSTR is LPWSTR which is UTF-16 */
  ret_text = g_utf16_to_utf8 (error_text, 0, NULL, NULL, NULL);
#else
  ret_text = g_strdup (error_text);
#endif

  LocalFree (error_text);
  return ret_text;
}
