/* GStreamer
 * Copyright (C) 2018 Sebastian Dröge <sebastian@centricular.com>
 * Copyright (C) 2021 Advance Software Limited <steve@advance-software.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstxaudio2device.h"

#include <windows.h>
#include <xaudio2.h>
#include <mmsystem.h>
#include <stdio.h>

#ifdef GST_XAUDIO2_SRC_DEVICE_PROVIDER
#include "gstxaudio2src.h"
#else
#include "gstxaudio2sink.h"
#endif


G_DEFINE_TYPE (GstXAudio2DeviceProvider, gst_xaudio2_device_provider,
    GST_TYPE_DEVICE_PROVIDER);

static GList *gst_xaudio2_device_provider_probe (GstDeviceProvider *
    provider);

static void
gst_xaudio2_device_provider_class_init (GstXAudio2DeviceProviderClass *
    klass)
{
  GstDeviceProviderClass *dm_class = GST_DEVICE_PROVIDER_CLASS (klass);

  gst_device_provider_class_set_static_metadata (dm_class,
#ifdef GST_XAUDIO2_SRC_DEVICE_PROVIDER
      "XAudio2 Source Device Provider", "Source/Audio",
      "List XAudio2 source devices",
#else
      "XAudio2 Sink Device Provider", "Sink/Audio",
      "List XAudio2 sink devices",
#endif
      "Steve Williams <steve@advance-software.com>");

  dm_class->probe = gst_xaudio2_device_provider_probe;
}

static void
gst_xaudio2_device_provider_init (GstXAudio2DeviceProvider * provider)
{
}

static gchar *
guid_to_string (LPGUID guid)
{
  gunichar2 *wstr = NULL;
  gchar *str = NULL;

  // ADV_SW_PATCH: DSound was wrong here :)
  if (StringFromCLSID( *guid, (LPOLESTR *) &wstr) == S_OK) {
    str = g_utf16_to_utf8 (wstr, -1, NULL, NULL, NULL);
    CoTaskMemFree (wstr);
  }

  return str;
}

typedef struct
{
  GstXAudio2DeviceProvider *self;
  GList **devices;
} ProbeData;

static BOOL CALLBACK
gst_xaudio2_enum_callback (GUID * pGUID, TCHAR * strDesc,
    TCHAR * strDrvName, VOID * pContext)
{
  ProbeData *probe_data = (ProbeData *) (pContext);
  gchar *driver, *description, *guid_str;
  GstStructure *props;
  GstDevice *device;
#ifdef GST_XAUDIO2_SRC_DEVICE_PROVIDER
  static GstStaticCaps caps = GST_STATIC_CAPS (GST_XAUDIO2_SRC_CAPS);
#else
  static GstStaticCaps caps = GST_STATIC_CAPS (GST_XAUDIO2_SINK_CAPS);
#endif

  description = g_locale_to_utf8 (strDesc, -1, NULL, NULL, NULL);
  if (!description) {
    GST_ERROR_OBJECT (probe_data->self,
        "Failed to convert description from locale encoding to UTF8");
    return TRUE;
  }

  driver = g_locale_to_utf8 (strDrvName, -1, NULL, NULL, NULL);
  if (!driver) {
    GST_ERROR_OBJECT (probe_data->self,
        "Failed to convert driver name from locale encoding to UTF8");
    return TRUE;
  }

  /* NULL for the primary sound card */
  guid_str = pGUID ? guid_to_string (pGUID) : NULL;

  GST_INFO_OBJECT (probe_data->self, "sound device name: %s, %s (GUID %s)",
      description, driver, GST_STR_NULL (guid_str));

  props = gst_structure_new ("xaudio2-proplist",
      "device.api", G_TYPE_STRING, "xaudio2",
      "device.guid", G_TYPE_STRING, GST_STR_NULL (guid_str),
      "xaudio2.device.driver", G_TYPE_STRING, driver,
      "xaudio2.device.description", G_TYPE_STRING, description, NULL);

#ifdef GST_XAUDIO2_SRC_DEVICE_PROVIDER
  device = g_object_new (GST_TYPE_XAUDIO2_DEVICE, "device-guid", guid_str,
      "display-name", description, "caps", gst_static_caps_get (&caps),
      "device-class", "Audio/Source", "properties", props, NULL);
#else
  device = (GstDevice *) g_object_new (GST_TYPE_XAUDIO2_DEVICE, "device-guid", guid_str,
      "display-name", description, "caps", gst_static_caps_get (&caps),
      "device-class", "Audio/Sink", "properties", props, NULL);
#endif

  *probe_data->devices = g_list_prepend (*probe_data->devices, device);

  g_free (description);
  g_free (driver);
  g_free (guid_str);

  gst_structure_free (props);

  return TRUE;
}

static GList *
gst_xaudio2_device_provider_probe (GstDeviceProvider * provider)
{
  GstXAudio2DeviceProvider *self =
      GST_XAUDIO2_DEVICE_PROVIDER (provider);

  GList *devices = NULL;
  ProbeData probe_data = { self, &devices };

  return devices;
}

enum
{
  PROP_DEVICE_GUID = 1,
};

G_DEFINE_TYPE (GstXAudio2Device, gst_xaudio2_device, GST_TYPE_DEVICE);

static void gst_xaudio2_device_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);
static void gst_xaudio2_device_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_xaudio2_device_finalize (GObject * object);
static GstElement *gst_xaudio2_device_create_element (GstDevice * device,
    const gchar * name);

static void
gst_xaudio2_device_class_init (GstXAudio2DeviceClass * klass)
{
  GstDeviceClass *dev_class = GST_DEVICE_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  dev_class->create_element = gst_xaudio2_device_create_element;

  object_class->get_property = gst_xaudio2_device_get_property;
  object_class->set_property = gst_xaudio2_device_set_property;
  object_class->finalize = gst_xaudio2_device_finalize;

  g_object_class_install_property (object_class, PROP_DEVICE_GUID,
      g_param_spec_string ("device-guid", "Device GUID",
          "Device GUID", NULL,
          (GParamFlags) (G_PARAM_STATIC_STRINGS | G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY) ));
}

static void
gst_xaudio2_device_init (GstXAudio2Device * device)
{
}

static void
gst_xaudio2_device_finalize (GObject * object)
{
  GstXAudio2Device *device = GST_XAUDIO2_DEVICE (object);

  g_free (device->guid);

  G_OBJECT_CLASS (gst_xaudio2_device_parent_class)->finalize (object);
}

static GstElement *
gst_xaudio2_device_create_element (GstDevice * device, const gchar * name)
{
  GstXAudio2Device *xaudio2_dev = GST_XAUDIO2_DEVICE (device);
  GstElement *elem;

#ifdef GST_XAUDIO2_SRC_DEVICE_PROVIDER
  elem = gst_element_factory_make ("xaudio2src", name);
#else
  elem = gst_element_factory_make ("xaudio2sink", name);
#endif

  g_object_set (elem, "device", xaudio2_dev->guid, NULL);

  return elem;
}

static void
gst_xaudio2_device_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstXAudio2Device *device = GST_XAUDIO2_DEVICE_CAST (object);

  switch (prop_id) {
    case PROP_DEVICE_GUID:
      g_value_set_string (value, device->guid);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_xaudio2_device_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstXAudio2Device *device = GST_XAUDIO2_DEVICE_CAST (object);

  switch (prop_id) {
    case PROP_DEVICE_GUID:
      device->guid = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}
