/* GStreamer XAudio2 Plugin
 * Copyright (C) 2018 Sebastian Dröge <sebastian@centricular.com>
 * Copyright (C) 2021 Advance Software Limited <steve@advance-software.com>
*
* gstxaudio2plugin.cpp:
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.

* You should have received a copy of the GNU Lesser General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
* 
*
*/

#define HAVE_CONFIG_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include "gstxaudio2sink.h"
#include "gstxaudio2device.h"

static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "xaudio2sink", GST_RANK_SECONDARY,
          GST_TYPE_XAUDIO2_SINK))
    return FALSE;

  if (!gst_device_provider_register (plugin, "xaudio2sinkdeviceprovider",
          GST_RANK_PRIMARY, GST_TYPE_XAUDIO2_DEVICE_PROVIDER))
    return FALSE;

  return TRUE;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    xaudio2,
    "XAudio2 plugin library",
    plugin_init, VERSION, "LGPL", GST_PACKAGE_NAME, GST_PACKAGE_ORIGIN)
